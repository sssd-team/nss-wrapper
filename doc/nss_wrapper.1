'\" t
.\"     Title: nss_wrapper
.\"    Author: Samba Team
.\" Generator: Asciidoctor 2.0.23
.\"      Date: 2024-07-05
.\"    Manual: \ \&
.\"    Source: \ \&
.\"  Language: English
.\"
.TH "NSS_WRAPPER" "1" "2024-07-05" "\ \&" "\ \&"
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.ss \n[.ss] 0
.nh
.ad l
.de URL
\fI\\$2\fP <\\$1>\\$3
..
.als MTO URL
.if \n[.g] \{\
.  mso www.tmac
.  am URL
.    ad l
.  .
.  am MTO
.    ad l
.  .
.  LINKSTYLE blue R < >
.\}
.SH "NAME"
nss_wrapper \- A wrapper for the user, group and hosts NSS API
.SH "SYNOPSIS"
.sp
LD_PRELOAD=libnss_wrapper.so NSS_WRAPPER_PASSWD=/path/to/passwd NSS_WRAPPER_GROUP=/path/to/group NSS_WRAPPER_HOSTS=/path/to/host \fB./myapplication\fP
.SH "DESCRIPTION"
.sp
There are projects which provide daemons needing to be able to create, modify
and delete Unix users. Or just switch user ids to interact with the system e.g.
a user space file server. To be able to test that you need the privilege to
modify the passwd and groups file. With nss_wrapper it is possible to define
your own passwd and groups file which will be used by software to act correctly
while under test.
.sp
If you have a client and server under test they normally use functions to
resolve network names to addresses (dns) or vice versa. The nss_wrappers allow
you to create a hosts file to setup name resolution for the addresses you use
with socket_wrapper.
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
Provides information for user and group accounts.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
Network name resolution using a hosts file.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
Loading and testing of NSS modules.
.RE
.SH "LIMITATIONS"
.sp
Some calls in nss_wrapper will only work if uid_wrapper is loaded and active.
One of this functions is initgroups() which needs to run setgroups() to set
the groups for the user. setgroups() is wrapped by uid_wrapper.
.SH "ENVIRONMENT VARIABLES"
.sp
\fBNSS_WRAPPER_PASSWD\fP, \fBNSS_WRAPPER_GROUP\fP
.RS 4
For user and group accounts you need to create two files: \fIpasswd\fP and \fIgroup\fP.
The format of the passwd file we support is:
.RE
.sp
name:password:UID:GID:GECOS:directory:shell
.sp
The format of the group file we support is:
.sp
group_name:password:GID:user_list
.sp
They are also described in \fIman passwd.5\fP and \fIman group.5\fP on Linux. You can
fill these files with made up accounts. You point
nss_wrapper to them using the two variables
NSS_WRAPPER_PASSWD=/path/to/your/passwd and
NSS_WRAPPER_GROUP=/path/to/your/group.
.sp
\fBNSS_WRAPPER_HOSTS\fP
.RS 4
If you also need to emulate network name resolution in your environment,
especially with socket_wrapper, you can write a hosts file. The format is
described in \fIman 5 hosts\fP. Then you can point nss_wrapper to your hosts
file using: NSS_WRAPPER_HOSTS=/path/to/your/hosts
.RE
.sp
\fBNSS_WRAPPER_HOSTNAME\fP
.RS 4
If you need to return a hostname which is different from the one of your
machine is using you can use: NSS_WRAPPER_HOSTNAME=test.example.org
.RE
.sp
\fBNSS_WRAPPER_MODULE_SO_PATH\fP, \fBNSS_WRAPPER_MODULE_FN_PREFIX\fP
.RS 4
If you have a project which also provides user and group information out of a
database, you normally write your own nss modules. nss_wrapper is able to load
nss modules and ask them first before looking into the faked passwd and group
file. To point nss_wrapper to the module you can do that using
NSS_WRAPPER_MODULE_SO_PATH=/path/to/libnss_yourmodule.so. As each nss module
has a special prefix like _nss_winbind_getpwnam() you need to set the prefix
too so nss_wrapper can load the functions with
NSS_WRAPPER_MODULE_FN_PREFIX=<prefix>.
.RE
.sp
For _nss_winbind_getpwnam() this would be:
.sp
.if n .RS 4
.nf
.fam C
NSS_WRAPPER_MODULE_FN_PREFIX=winbind
.fam
.fi
.if n .RE
.sp
\fBNSS_WRAPPER_DEBUGLEVEL\fP
.RS 4
If you need to see what is going on in nss_wrapper itself or try to find a
bug, you can enable logging support in nss_wrapper if you built it with
debug symbols.
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
0 = ERROR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
1 = WARNING
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
2 = DEBUG
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.  sp -1
.  IP \(bu 2.3
.\}
3 = TRACE
.RE
.RE
.sp
\fBNSS_WRAPPER_DISABLE_DEEPBIND\fP
.RS 4
This allows you to disable deep binding in nss_wrapper. This is useful for
running valgrind tools or sanitizers like (address, undefined, thread).
.RE
.SH "EXAMPLE"
.sp
.if n .RS 4
.nf
.fam C
$ echo "bob:x:1000:1000:bob gecos:/home/test/bob:/bin/false" > passwd
$ echo "root:x:65534:65532:root gecos:/home/test/root:/bin/false" >> passwd
$ echo "users:x:1000:" > group
$ echo "root:x:65532:" >> group
$ LD_PRELOAD=libnss_wrapper.so NSS_WRAPPER_PASSWD=passwd \(rs
  NSS_WRAPPER_GROUP=group getent passwd bob
bob:x:1000:1000:bob gecos:/home/test/bob:/bin/false
$ LD_PRELOAD=libnss_wrapper.so NSS_WRAPPER_HOSTNAME=test.example.org hostname
test.example.org
.fam
.fi
.if n .RE
.SH "AUTHOR"
.sp
Samba Team